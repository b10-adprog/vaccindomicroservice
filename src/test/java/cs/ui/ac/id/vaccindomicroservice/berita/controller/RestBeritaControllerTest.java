package cs.ui.ac.id.vaccindomicroservice.berita.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindomicroservice.berita.service.BeritaServiceImpl;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetails;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetailsService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = RestBeritaControllerAdmin.class)
public class RestBeritaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BeritaServiceImpl beritaService;

    @MockBean
    CustomPendaftarDetails pd;

    @MockBean
    CustomPendaftarDetailsService cpds;

    @MockBean
    PendaftarRepository pendaftarRepository;

    @MockBean
    PendaftarService pendaftarService;

    private Berita beritaNasional;

    @BeforeEach
    public void setUp() {
        beritaNasional = new Berita();
        beritaNasional.setIdBerita(1);
        beritaNasional.setJudul("Judul");
        beritaNasional.setKategori(KategoriBerita.NASIONAL);
        beritaNasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaNasional.setDeskripsi("Deskripsi");
        beritaNasional.setUrlGambar("url//ke/sesuatu.png");
    }

    @Test
    @WithMockUser("admin")
    void testControllerGetExistingBerita() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        when(beritaService.getBerita(anyString())).thenReturn(beritaNasional);

        mockMvc.perform(get("/admin/berita/cek").param("judulBerita", "Judul"))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().string("Judul"));
    }

    @Test
    @WithMockUser("admin")
    public void testControllerGetNonExistingBerita() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/berita/cek").param("judulBerita", "JudulBelumAda"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithMockUser("admin")
    public void testControllerDeleteBerita() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/berita/delete/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/admin/berita/cek?judulBerita=Judul"))
                .andExpect(status().isNotFound());
    }
}
