package cs.ui.ac.id.vaccindomicroservice.hospital.service;

import cs.ui.ac.id.vaccindomicroservice.hospital.model.Hospital;
import cs.ui.ac.id.vaccindomicroservice.hospital.repository.HospitalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HospitalServiceTest {

    @Mock
    HospitalRepository hospitalRepository;

    @InjectMocks
    HospitalServiceImpl hospitalService;

    private Hospital hospital;
    private List<Hospital> hospitalList;

    @BeforeEach
    public void setUp() {
        hospital = new Hospital();
        hospital.setIdHospital(1);
        hospital.setHospitalName("RS Citra Medika");
        hospital.setHospitalAddress("Jalan Raya Lenteng Agung no. 54");
        hospital.setHospitalDescription("Rumah sakit kepercayaan keluarga");
        hospitalList = new ArrayList<>();
        hospitalList.add(hospital);
    }

    @Test
    public void getAllHospitalReturnsHospitalList() {
        when(hospitalService.getAllHospital()).thenReturn(hospitalList);

        List<Hospital> listHospitalFromMock = hospitalService.getAllHospital();

        assertEquals(listHospitalFromMock.size(), 1);
        verify(hospitalRepository, times(1)).findAll();
    }

    @Test
    public void addHospitalToListHospital() {
        Hospital hospitalDummy = new Hospital();
        hospitalDummy.setIdHospital(1);
        hospitalDummy.setHospitalName("Rumah Sakit Setiabudi");
        hospitalDummy.setHospitalAddress("Jalan Raya Pasar Minggu");
        hospitalDummy.setHospitalDescription("Rumah sakit cinta keluarga");

        hospitalService.addHospital(hospitalDummy);

        verify(hospitalRepository, times(1)).save(any(Hospital.class));
    }

    @Test
    public void getHospitalByIdReturnsCorrectHospital() {
        when(hospitalService.getHospitalById(anyInt())).thenReturn(hospital);

        Hospital hospitalDummy = hospitalService.getHospitalById(1);
        assertEquals(hospitalDummy.getHospitalName(), "RS Citra Medika");

        verify(hospitalRepository, times(1)).findHospitalByIdHospital(1);
    }

    @Test
    public void getHospitalByNameReturnsCorrectHospital() {
        when(hospitalService.getHospitalByName(anyString())).thenReturn(hospital);

        Hospital hospitalDummy = hospitalService.getHospitalByName("RS Citra Medika");
        assertEquals(hospitalDummy.getIdHospital(), 1);

        verify(hospitalRepository, times(1)).findHospitalByHospitalName("RS Citra Medika");
    }
}
