package cs.ui.ac.id.vaccindomicroservice.historypendaftaran.controller;

import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.service.HistoryPendaftaranService;
import cs.ui.ac.id.vaccindomicroservice.hospital.service.HospitalService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetails;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetailsService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = HistoryPendaftaranController.class)
public class HistoryPendaftaranControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HistoryPendaftaranService historyPendaftaranService;

    @MockBean
    HospitalService hospitalService;

    @MockBean
    PendaftarRepository pendaftarRepository;

    @MockBean
    PendaftarService pendaftarService;

    @MockBean
    CustomPendaftarDetails pd;

    @MockBean
    CustomPendaftarDetailsService cpds;

    private HistoryPendaftaran historyPendaftaran;
    private Pendaftar pendaftar;

    @BeforeEach
    public void setUp() {
        pendaftar = new Pendaftar();
        pendaftar.setNama("Gita");
        pendaftar.setNik("1234567890");
        pendaftar.setRegistered(true);

        historyPendaftaran = new HistoryPendaftaran();
        historyPendaftaran.setTanggal("4 April 2021");
        historyPendaftaran.setRumahSakit("RSUI");
        historyPendaftaran.setPendaftar(pendaftar);
    }

    @Test
    @WithMockUser("admin")
    public void getHPUrlByGetAllUnfinishedHistoryPendaftaran() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password123");
        admin.setIsAdmin(true);
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/pendaftaran"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getListPendaftaran"))
                .andExpect(model().attributeExists("unfinishedPendaftaran"))
                .andExpect(view().name("historypendaftaran/listPendaftaran"));
        verify(historyPendaftaranService, times(1)).getListPendaftaranUnfinished();
    }

    @Test
    @WithMockUser
    public void testDaftarVaksinasi() throws Exception{
        Pendaftar user = new Pendaftar();
        user.setUsername("user");
        user.setPassword("password123");
        when(pendaftarService.getPendaftarByUsername("user")).thenReturn(user);

        mockMvc.perform(get("/daftar-vaksinasi"))
                .andExpect(status().isOk())
                .andExpect(view().name("historypendaftaran/daftarVaksinasi"));
    }

    @Test
    @WithMockUser("admin")
    public void testPostPendaftaranSetuju() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password404");
        admin.setIsAdmin(true);
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(post("/pendaftaran/" + historyPendaftaran.getIdHistoryPendaftaran()).param("setuju", "setuju"))
                .andExpect(redirectedUrl("/admin/pendaftaran"));
        verify(historyPendaftaranService, times(1)).updateApprovalStatus(0, true);
        verify(historyPendaftaranService, times(1)).updateFinishedStatus(0, true);
    }

    @Test
    @WithMockUser("admin")
    public void testPostPendaftaranTolak() throws Exception{
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password404");
        admin.setIsAdmin(true);
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(post("/pendaftaran/" + historyPendaftaran.getIdHistoryPendaftaran()).param("tolak", "tolak"))
                .andExpect(redirectedUrl("/admin/pendaftaran"));
        verify(historyPendaftaranService, times(1)).updateApprovalStatus(0, false);
        verify(historyPendaftaranService, times(1)).updateFinishedStatus(0, true);
    }

    @Test
    @WithMockUser("user")
    public void testPostDaftarVaksinasi() throws Exception {
        Pendaftar user = new Pendaftar();
        user.setUsername("user");
        user.setPassword("password404");

        when(pendaftarService.getPendaftarByUsername("user")).thenReturn(user);

        mockMvc.perform(post("/daftar-vaksinasi"))
                .andExpect(redirectedUrl("/profile"));
    }

    @Test
    @WithMockUser
    public void testProfile() throws Exception {
        pendaftar.setUsername("pendaftar");
        pendaftar.setPassword("password123");
        pendaftar.setUsername("user");
        when(pendaftarService.getPendaftarByUsername("user")).thenReturn(pendaftar);

        mockMvc.perform(get("/profile/"))
                .andExpect(model().attributeExists("allHistoryPendaftaran"))
                .andExpect(view().name("historypendaftaran/profile"));
        verify(historyPendaftaranService, times(1)).getAllHistoryPendaftaranOfAProfile(pendaftar.getNik());
    }

}
