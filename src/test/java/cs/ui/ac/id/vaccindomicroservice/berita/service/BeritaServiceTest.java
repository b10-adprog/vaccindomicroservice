package cs.ui.ac.id.vaccindomicroservice.berita.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindomicroservice.berita.repository.BeritaRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarServiceImpl;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BeritaServiceTest {
    @Mock
    BeritaRepository beritaRepository;

    @Mock
    PendaftarServiceImpl pendaftarService;

    @InjectMocks
    BeritaServiceImpl beritaService;

    private Berita beritaNasional;
    private Berita beritaInternasional;
    private List<Berita> listBerita;

    @BeforeEach
    public void setUp() {
        beritaNasional = new Berita();
        beritaNasional.setIdBerita(1);
        beritaNasional.setJudul("Judul");
        beritaNasional.setKategori(KategoriBerita.NASIONAL);
        beritaNasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaNasional.setDeskripsi("Deskripsi");
        beritaNasional.setUrlGambar("url//ke/sesuatu.png");

        beritaInternasional = new Berita();
        beritaInternasional.setIdBerita(2);
        beritaInternasional.setJudul("Title");
        beritaInternasional.setKategori(KategoriBerita.INTERNASIONAL);
        beritaInternasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaInternasional.setDeskripsi("Description");
        beritaInternasional.setUrlGambar("url//to/someting.png");
        
        listBerita = new ArrayList<>();
        listBerita.add(beritaNasional);
        listBerita.add(beritaInternasional);
    }

    @Test
    public void getBeritaByIdReturnsCorrectBerita() {
        when(beritaService.getBerita(anyInt())).thenReturn(beritaNasional);

        Berita beritaFromMock = beritaService.getBerita(anyString(), 1);

        assertEquals(beritaFromMock.getJudul(), "Judul");
        verify(beritaRepository, times(1)).findBeritaByIdBerita(1);
    }

    @Test
    public void getBeritaByJudulReturnsCorrectBerita() {
        when(beritaService.getBerita(anyString())).thenReturn(beritaNasional);

        Berita beritaFromMock = beritaService.getBerita(anyString());

        assertEquals(beritaFromMock.getJudul(), "Judul");
        verify(beritaRepository, times(1)).findBeritaByJudul(anyString());
    }

    @Test
    public void getAllBeritaNoArgReturnsAllBeritaList() {
        when(beritaService.getAllBerita()).thenReturn(listBerita);

        List<Berita> listBeritaFromMock = beritaService.getAllBerita();

        assertEquals(listBeritaFromMock.size(), 2);
        verify(beritaRepository, times(1)).findAll();
    }

    @Test
    public void getAllBeritaWithNikArgReturnSubscribedBeritaListOneCategory() {
        Pendaftar pendaftar1 = new Pendaftar(
                "nitami", "1906350894", "Nitami", "password123", false
        );
        pendaftar1.setSubbedToBeritaInternasional(false);
        pendaftar1.setSubbedToBeritaNasional(true);
        pendaftar1.setRegistered(true);

        Pendaftar pendaftar2 = new Pendaftar("maung", "1906000000", "Maung", "password123", false);
        pendaftar2.setSubbedToBeritaInternasional(true);
        pendaftar2.setSubbedToBeritaNasional(false);
        pendaftar2.setRegistered(true);

        List<Berita> listBeritaNasional = new ArrayList<>();
        listBeritaNasional.add(beritaNasional);
        List<Berita> listBeritaInternasional = new ArrayList<>();
        listBeritaInternasional.add(beritaInternasional);

        when(beritaRepository.findBeritasByKategoriIs(KategoriBerita.NASIONAL))
                .thenReturn(listBeritaNasional);
        when(beritaRepository.findBeritasByKategoriIs(KategoriBerita.INTERNASIONAL))
                .thenReturn(listBeritaInternasional);
        when(pendaftarService.getPendaftarByUsername("nitami"))
                .thenReturn(pendaftar1);
        when(pendaftarService.getPendaftarByUsername("maung"))
                .thenReturn(pendaftar2);
        when(beritaService.getAllBerita("nitami"))
                .thenReturn(listBeritaNasional);
        when(beritaService.getAllBerita("maung"))
                .thenReturn(listBeritaInternasional);

        List<Berita> listBeritaFromMock = new ArrayList<>();
        listBeritaFromMock = beritaService.getAllBerita("nitami");

        assertEquals(listBeritaFromMock.size(), 1);
        assertEquals(listBeritaFromMock, listBeritaNasional);
        assertNotEquals(listBeritaFromMock, listBeritaInternasional);

        verify(beritaRepository, times(1)).findBeritasByKategoriIs(KategoriBerita.NASIONAL);

        listBeritaFromMock = beritaService.getAllBerita("maung");

        assertEquals(listBeritaFromMock.size(), 1);
        assertNotEquals(listBeritaFromMock, listBeritaNasional);
        assertEquals(listBeritaFromMock, listBeritaInternasional);

        verify(beritaRepository, times(1)).findBeritasByKategoriIs(KategoriBerita.INTERNASIONAL);
    }

    @Test
    public void getAllBeritaWithUsernameReturnSubscribedBeritaListBothCategory() {
        Pendaftar pendaftar3 = new Pendaftar("maung", "1906000000", "Maung", "password123", false);
        pendaftar3.setSubbedToBeritaInternasional(true);
        pendaftar3.setSubbedToBeritaNasional(true);
        pendaftar3.setRegistered(true);

        when(pendaftarService.getPendaftarByUsername(anyString())).thenReturn(pendaftar3);
        when(beritaService.getAllBerita(anyString())).thenReturn(listBerita);

        List<Berita> listBeritaFromMock = beritaService.getAllBerita("maung");

        assertEquals(listBeritaFromMock.size(), 2);
        assertEquals(listBeritaFromMock, listBerita);

        verify(beritaRepository, times(3)).findBeritasByKategoriIs(any(KategoriBerita.class));
    }

    @Test
    public void deleteBeritaMakesListBeritaEmpty() {
        when(beritaService.getBerita(1)).thenReturn(null);

        beritaService.deleteBerita(1);
        verify(beritaRepository, times(1)).deleteBeritaByIdBerita(any(Integer.class));

        assertNotEquals(beritaService.getBerita(1), beritaNasional);
    }

    @Test
    public void addBeritaAddsOneToListBerita() {
        Berita berita2 = new Berita();
        berita2.setIdBerita(2);
        berita2.setJudul("Judul");
        berita2.setKategori(KategoriBerita.NASIONAL);
        berita2.setTimeStamp(LocalDateTime.of(2018, Month.FEBRUARY, 2, 12, 0));
        berita2.setDeskripsi("Deskripsi");
        berita2.setUrlGambar("url//to/picture.jpg");

        beritaService.addBerita(berita2);

        verify(beritaRepository, times(1)).save(any(Berita.class));
    }
}
