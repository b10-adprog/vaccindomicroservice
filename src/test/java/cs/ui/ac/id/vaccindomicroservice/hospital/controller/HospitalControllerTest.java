package cs.ui.ac.id.vaccindomicroservice.hospital.controller;

import cs.ui.ac.id.vaccindomicroservice.hospital.model.Hospital;
import cs.ui.ac.id.vaccindomicroservice.hospital.service.HospitalService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetails;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetailsService;
import java.util.ArrayList;
import java.util.List;

import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = HospitalController.class)
public class HospitalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HospitalService hospitalService;

    @MockBean
    PendaftarRepository pendaftarRepository;

    @MockBean
    PendaftarService pendaftarService;

    @MockBean
    CustomPendaftarDetails pd;

    @MockBean
    CustomPendaftarDetailsService cpds;

    @Test
    @WithMockUser("admin")
    public void getInputHospitalFormPage() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        Hospital hospital = new Hospital();
        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/hospital/input/")
        .param("hospital", String.valueOf(hospital))
        ).andExpect(status().isOk())
                .andExpect(model().attributeExists("hospital"))
                .andExpect(handler().methodName("inputHospital"))
                .andExpect(view().name("hospital/inputHospital"));
    }

//    @Test
//    @WithMockUser("admin")
//    public void postInputHospitalRedirectToListHospitalPage() throws Exception {
//        Pendaftar admin = new Pendaftar();
//        admin.setUsername("admin");
//        admin.setPassword("password001");
//        admin.setIsAdmin(true);
//
//        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);
//
//        Hospital hospital = new Hospital("RS Citra Medika", "Jalan Raya Lenteng Agung no. 54", "Deskripsi");
//        hospital.setIdHospital(1);
//        assertNotNull(hospital);
//
//        List<Hospital> hospitalList = new ArrayList<>();
//        hospitalList.add(hospital);
//
//        when(hospitalService.getAllHospital()).thenReturn(hospitalList);
//
//        mockMvc.perform(post("/admin/hospital/inputSave")
//                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
//                .accept(MediaType.APPLICATION_FORM_URLENCODED)
//                .param("hospitalName", hospital.getHospitalName())
//                .param("hospitalAddress", hospital.getHospitalAddress())
//        ).andExpect(status().is3xxRedirection())
//                .andExpect(handler().methodName("inputHospital"))
//                .andExpect(redirectedUrl("/admin/hospital/"));
//        verify(hospitalService, times(1)).addHospital(any(Hospital.class));
//    }

    @Test
    @WithMockUser("admin")
    public void getAllListHospital() throws Exception {
        Pendaftar admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/hospital/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("listHospital"))
                .andExpect(model().attributeExists("listHospital"))
                .andExpect(view().name("hospital/listHospital"));
        verify(hospitalService, times(1)).getAllHospital();
    }

}
