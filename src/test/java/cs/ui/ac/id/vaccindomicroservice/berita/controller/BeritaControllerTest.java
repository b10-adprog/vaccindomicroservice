package cs.ui.ac.id.vaccindomicroservice.berita.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindomicroservice.berita.service.BeritaService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetails;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.CustomPendaftarDetailsService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = {BeritaControllerUser.class, BeritaControllerAdmin.class})
public class BeritaControllerTest {

    private Berita beritaNasional;
    private Berita beritaInternasional;
    private Pendaftar admin;
    private Pendaftar user;

    @MockBean
    private BeritaService beritaService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CustomPendaftarDetails pd;

    @MockBean
    CustomPendaftarDetailsService cpds;

    @MockBean
    PendaftarRepository pendaftarRepository;

    @MockBean
    PendaftarService pendaftarService;

    @BeforeEach
    public void setUp() {
        beritaNasional = new Berita();
        beritaNasional.setIdBerita(1);
        beritaNasional.setJudul("Judul");
        beritaNasional.setKategori(KategoriBerita.NASIONAL);
        beritaNasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaNasional.setDeskripsi("Deskripsi");
        beritaNasional.setUrlGambar("url//ke/sesuatu.png");

        beritaInternasional = new Berita();
        beritaInternasional.setIdBerita(2);
        beritaInternasional.setJudul("Title");
        beritaInternasional.setKategori(KategoriBerita.INTERNASIONAL);
        beritaInternasional.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        beritaInternasional.setDeskripsi("Description");
        beritaInternasional.setUrlGambar("url//to/someting.png");
    }

    @Test
    @WithMockUser(value = "nitami")
    public void getBeritaUrlByPenggunaCallGetAllBerita() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);
        mockMvc.perform(get("/berita"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaPengguna"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(model().attributeExists("username"))
                .andExpect(view().name("berita/lihatBerita"));
        verify(beritaService, times(1)).getAllBerita(any(String.class));
    }

    @Test
    @WithMockUser(value = "nitami")
    public void getLihatBeritaUrlByPenggunaCallGetAllBerita() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);

        mockMvc.perform(get("/berita/lihat"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaPengguna"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(model().attributeExists("username"))
                .andExpect(view().name("berita/lihatBerita"));
        verify(beritaService, times(1)).getAllBerita(any(String.class));
    }

    @Test
    @WithMockUser(value = "nitami")
    public void getUbahLanggananBeritaUrlByPenggunaReturnUbahLanggananView() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);

        mockMvc.perform(get("/berita/ubah-langganan"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("ubahLanggananPengguna"))
                .andExpect(model().attributeExists("pendaftar"))
                .andExpect(view().name("berita/ubahLangganan"));
    }

    @Test
    @WithMockUser(value = "nitami")
    public void postUbahLanggananBeritaUrlByPenggunaRedirectToBerita() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");
        user.setRegistered(true);
        user.setSubbedToBeritaNasional(true);
        user.setSubbedToBeritaInternasional(true);

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);

        mockMvc.perform(post("/berita/ubah-langganan")
                    .param("NasionalSubOptions", "false")
                    .param("InternasionalSubOptions", "false")
                )
                .andExpect(handler().methodName("ubahLanggananPenggunaPost"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/berita/lihat"));
    }

    @Test
    @WithMockUser(value = "nitami")
    public void getLihatDetailBeritaValidUrlByPenggunaCallGetBerita() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);

        assertEquals("2020-11-01 12:00", beritaNasional.getTimeStamp());
        when(beritaService.getBerita(any(String.class), any(Integer.class)))
                .thenReturn(beritaNasional);

        mockMvc.perform(get("/berita/lihat/detail/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("detailBeritaPengguna"))
                .andExpect(model().attributeExists("berita"))
                .andExpect(view().name("berita/detailBerita"));
        verify(beritaService, times(1)).getBerita(any(String.class), any(Integer.class));
    }

    @Test
    @WithMockUser(value = "nitami")
    public void getLihatDetailBeritaInvalidUrlByPenggunaCallGetBerita() throws Exception {
        user = new Pendaftar();
        user.setUsername("nitami");
        user.setPassword("password123");

        when(pendaftarService.getPendaftarByUsername("nitami")).thenReturn(user);

        mockMvc.perform(get("/berita/lihat/detail/judul"))
                .andExpect(status().isNotFound());
        verify(beritaService, times(0))
                .getBerita(any(String.class), any(Integer.class));
    }

    @Test
    @WithMockUser(value = "admin")
    public void getTulisBeritaSyncUrlByAdminReturnsFormPage() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        Berita berita = new Berita();
        mockMvc.perform(get("/admin/berita/tulisSync/")
                .param("berita", String.valueOf(berita)))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("berita"))
                .andExpect(handler().methodName("tulisBeritaAdminSync"))
                .andExpect(view().name("berita/tulisBeritaSync")
                );
    }

    @Test
    @WithMockUser(value = "admin")
    public void getTulisBeritaAsyncUrlByAdminReturnsFormPage() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        Berita berita = new Berita();
        mockMvc.perform(get("/admin/berita/tulis/")
                    .param("berita", String.valueOf(berita))
                ).andExpect(status().isOk())
                .andExpect(model().attributeExists("berita"))
                .andExpect(handler().methodName("tulisBeritaAdminAsync"))
                .andExpect(view().name("berita/tulisBeritaAsync")
        );
    }

    @Test
    @WithMockUser(value = "admin")
    public void postTulisBeritaUrlByAdminRedirectsToListBeritaPage() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        Berita berita = new Berita(
                "Berita nasional",
                KategoriBerita.NASIONAL,
                "url//to/someting.png",
                "Deskripsi");
        berita.setIdBerita(1);
        berita.setTimeStamp(LocalDateTime.of(2020, Month.NOVEMBER, 1, 12, 0));
        assertNotNull(berita);

        List<Berita> listBerita = new ArrayList<>();
        listBerita.add(berita);

        when(beritaService.getAllBerita()).thenReturn(listBerita);

        mockMvc.perform(post("/admin/berita/simpan/")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("judul", berita.getJudul())
                    .param("kategori", "NASIONAL")
                    .param("deskripsi", berita.getDeskripsi())
                ).andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("simpanBeritaAdmin"))
                .andExpect(redirectedUrl("/admin/berita/lihat/"));
        verify(beritaService, times(1)).addBerita(any(Berita.class));
    }

    @Test
    @WithMockUser(value = "admin")
    public void postTulisBeritaUrlInvalidByAdminReturnsBeritaPage() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(post("/admin/berita/simpan/")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_FORM_URLENCODED)
                ).andExpect(handler().methodName("simpanBeritaAdmin"))
                .andExpect(view().name("berita/tulisBeritaAsync"));
        verify(beritaService, times(0)).addBerita(any(Berita.class));
    }

    @Test
    @WithMockUser(value = "admin")
    public void getListBeritaUrlByAdminCallGetAllBerita() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/berita/lihat"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaAdmin"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(view().name("berita/listBeritaAsync"));
        verify(beritaService, times(1)).getAllBerita();
    }

    @Test
    @WithMockUser("admin")
    public void getBeritaUrlByAdminCallGetAllBerita() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/berita"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("lihatBeritaAdmin"))
                .andExpect(model().attributeExists("listBerita"))
                .andExpect(view().name("berita/listBeritaAsync"));
        verify(beritaService, times(1)).getAllBerita();
    }

    @Test
    @WithMockUser("admin")
    public void deleteBeritaUrlByAdminThenRedirectToListBeritaPage() throws Exception {
        admin = new Pendaftar();
        admin.setUsername("admin");
        admin.setPassword("password001");
        admin.setIsAdmin(true);

        when(pendaftarService.getPendaftarByUsername("admin")).thenReturn(admin);

        mockMvc.perform(get("/admin/berita/deleteSync/1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("deleteBeritaAdmin"))
                .andExpect(redirectedUrl("/admin/berita/lihat"));
        verify(beritaService, times(1)).deleteBerita(any(Integer.class));
    }
}
