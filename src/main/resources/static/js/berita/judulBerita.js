$(document).ready(function () {

console.log("script loaded");

$("#judul").keyup(() => ajaxCekJudul())

function ajaxCekJudul() {
    console.log("function set");

    var data = $("#judul").val();
    $.ajax({
        type: "GET",
        url: "/admin/berita/cek?judulBerita=" + data,
        success: function (data) {
            console.log("CONFLICT: ", data)
            $(":input").empty()
        },
        error: function (e) {
            if (e.status === 404) {
                console.log("NO CONFLICT: new berita");
            } else if (e.status === 200) {
                console.log("CONFLICT: berita exist")
                $("#judul-alert")
                    .text("Berita dengan judul sama sudah ada.")
                    .show()
                $("#judul").val("")
                setTimeout(() => {
                    $("#judul-alert").hide()
                },3000)
            }
        }
    })
}
})
