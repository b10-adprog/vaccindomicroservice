$(document).ready(function () {
    $(".btn-outline-danger").on("click", (e) => {
        ajaxDeleteBerita(
            $(e.target).parent().parent(),
            ($(e.target).parent().siblings()[1]).innerHTML
        )
    })

    function ajaxDeleteBerita(row, id) {
        if (!confirm("Hapus berita ini?\nID berita: " + id)) {
            return
        }
        $.ajax({
            type: "GET",
            url: "/admin/berita/delete/" + id,
            success: function () {
                console.log("DELETE: id berita ", id)
                row.remove()
            },
            error: function (e) {
                console.log("ERROR: ", e)
            }
        })
    }
})
