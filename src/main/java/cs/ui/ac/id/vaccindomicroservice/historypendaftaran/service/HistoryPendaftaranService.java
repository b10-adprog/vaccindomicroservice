package cs.ui.ac.id.vaccindomicroservice.historypendaftaran.service;

import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.model.HistoryPendaftaran;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.Future;

public interface HistoryPendaftaranService {
    HistoryPendaftaran createHistoryPendaftaran(HistoryPendaftaran history);
    List<HistoryPendaftaran> getAllHistoryPendaftaran();
    List<HistoryPendaftaran> getAllHistoryPendaftaranOfAProfile(String idProfile);
    HistoryPendaftaran getHistoryPendaftaran(Integer idHistoryPendaftaran);
    List<HistoryPendaftaran> getListPendaftaranUnfinished();
    void updateApprovalStatus(int idHistoryPendaftaran, boolean isVaccinated);
    void updateFinishedStatus(int idHistoryPendaftaran, boolean isVaccinated);
    void daftarVaksinasi(HistoryPendaftaran historyPendaftaran);
    boolean getPendaftarSecondVaccinated(int idHistoryPendaftaran);
    Future<Boolean> pendaftarFullyVaccinated();
    boolean getHistoryPendaftaranPastTime(int idHistoryPendaftaran) throws ParseException;
    Future<Boolean> pendaftarTimePassed();
}
