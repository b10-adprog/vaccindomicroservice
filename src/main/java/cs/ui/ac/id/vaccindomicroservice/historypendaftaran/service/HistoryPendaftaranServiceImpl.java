package cs.ui.ac.id.vaccindomicroservice.historypendaftaran.service;

import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.repository.HistoryPendaftaranRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class HistoryPendaftaranServiceImpl implements HistoryPendaftaranService{

    @Autowired
    HistoryPendaftaranRepository historyPendaftaranRepository;

    @Autowired
    PendaftarService pendaftarService;

    @Override
    public HistoryPendaftaran createHistoryPendaftaran(HistoryPendaftaran history) {
        historyPendaftaranRepository.save(history);
        return history;
    }

    @Override
    public List<HistoryPendaftaran> getAllHistoryPendaftaran() {
        return historyPendaftaranRepository.findAll();
    }

    @Override
    public List<HistoryPendaftaran> getAllHistoryPendaftaranOfAProfile(String idProfile) {
        List<HistoryPendaftaran> historyPendaftaranList = new ArrayList<>();
        for (HistoryPendaftaran history : historyPendaftaranRepository.findAll()) {
            if (history.getIdPendaftar().equals(idProfile)) {
                historyPendaftaranList.add(history);
            }
        }
        return historyPendaftaranList;
    }

    @Override
    public HistoryPendaftaran getHistoryPendaftaran(Integer idHistoryPendaftaran) {
        return historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
    }

    @Override
    public void updateApprovalStatus(int idHistoryPendaftaran, boolean isApproved) {
        HistoryPendaftaran history =
                historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
        history.setApproved(isApproved);

        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(history.getIdPendaftar());

        if (history.getApproved()) {
            updateStatusPendaftar(pendaftar);
        }

        history.getPendaftar().setRegistered(false);

        historyPendaftaranRepository.save(history);
    }

    private void updateStatusPendaftar(Pendaftar pendaftar) {
        for (Pendaftar p : pendaftarService.getListPendaftar()) {
            if (p.getNik().equals(pendaftar.getNik())) {
                if (!p.isFirstVaccinated() && !p.isSecondVaccinated()) {
                    pendaftar.setFirstVaccinated(true);
                } else if (p.isFirstVaccinated()) {
                    pendaftar.setSecondVaccinated(true);
                }
            }
        }

        if (!pendaftar.isFirstVaccinated() && !pendaftar.isSecondVaccinated()) {
            pendaftar.setFirstVaccinated(true);
        }
    }

    @Override
    public void updateFinishedStatus(int idHistoryPendaftaran, boolean isFinished) {
        HistoryPendaftaran history =
                historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
        history.setFinished(isFinished);

        if (history.getFinished()) {
            getAllHistoryPendaftaran().removeIf(h -> h == history);
        }

        historyPendaftaranRepository.save(history);
    }

    @Override
    public List<HistoryPendaftaran> getListPendaftaranUnfinished() {
        return historyPendaftaranRepository.findAllByFinishedIs(false);
    }

    @Override
    public void daftarVaksinasi(HistoryPendaftaran historyPendaftaran) {
        String nik = historyPendaftaran.getNik();
        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(nik);
        if (pendaftar != null) {
            if (!pendaftar.isRegistered()){
                pendaftar.setRegistered(true);
                historyPendaftaran.setPendaftar(pendaftar);
                historyPendaftaranRepository.save(historyPendaftaran);
            }
        }
    }

    public boolean getPendaftarSecondVaccinated(int idHistoryPendaftaran) {
        HistoryPendaftaran history =
                historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);

        Pendaftar pendaftar = pendaftarService.getPendaftarByNik(history.getIdPendaftar());

        return pendaftar.isSecondVaccinated();
    }

    @Async("executor")
    public Future<Boolean> pendaftarFullyVaccinated() {
        try {
            Thread.sleep(1);
            return new AsyncResult<>(true);
        } catch (Exception e) {

        }
        return null;
    }

    public boolean getHistoryPendaftaranPastTime(int idHistoryPendaftaran) throws ParseException {
        HistoryPendaftaran history =
                historyPendaftaranRepository.findByIdHistoryPendaftaran(idHistoryPendaftaran);
        String tanggal = history.getTanggal();
        Date pemesanan = new SimpleDateFormat("yyyy-MM-dd").parse(tanggal);

        LocalDate now = LocalDate.now();
        Date dateNow = java.sql.Date.valueOf(now);
        if (dateNow.compareTo(pemesanan) > 0){
            return true;
        }
        return false;
    }

    @Async("executor")
    public Future<Boolean> pendaftarTimePassed() {
        try {
            Thread.sleep(1);
            return new AsyncResult<>(true);
        } catch (InterruptedException e) {
        }
        return null;
    }
}
