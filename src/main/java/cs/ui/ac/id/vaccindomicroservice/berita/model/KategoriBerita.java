package cs.ui.ac.id.vaccindomicroservice.berita.model;

public enum KategoriBerita {
    NASIONAL,
    INTERNASIONAL
}
