package cs.ui.ac.id.vaccindomicroservice.hospital.service;

import cs.ui.ac.id.vaccindomicroservice.hospital.model.Hospital;
import cs.ui.ac.id.vaccindomicroservice.hospital.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService {

    @Autowired
    HospitalRepository hospitalRepository;

    @Override
    public List<Hospital> getAllHospital() {
        List<Hospital> listHospital = hospitalRepository.findAll();
        return listHospital;
    }

    @Override
    public Hospital getHospitalById(Integer idHospital) {
        Hospital hospital = hospitalRepository.findHospitalByIdHospital(idHospital);
        return hospital;
    }

    @Override
    public Hospital getHospitalByName(String name) {
        Hospital hospital = hospitalRepository.findHospitalByHospitalName(name);
        return hospital;
    }

    @Override
    @Transactional
    public void addHospital(Hospital hospital) {
        hospitalRepository.save(hospital);
    }
}
