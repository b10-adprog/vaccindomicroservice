package cs.ui.ac.id.vaccindomicroservice.berita.controller;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.service.BeritaService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/berita")
public class BeritaControllerUser {
    @Autowired
    BeritaService beritaService;

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path = {"", "/lihat"})
    public String lihatBeritaPengguna(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        List<Berita> listBerita = beritaService.getAllBerita(userSession.getUsername());
        model.addAttribute("listBerita", listBerita);
        model.addAttribute("username", userSession.getUsername());
        return "berita/lihatBerita";
    }

    @GetMapping(path = {"/ubah-langganan"})
    public String ubahLanggananPengguna(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        model.addAttribute("username", userSession.getUsername());
        Pendaftar pendaftar = pendaftarService.getPendaftarByUsername(userSession.getUsername());
        model.addAttribute("pendaftar", pendaftar);
        return "berita/ubahLangganan";
    }

    @PostMapping(path = {"/ubah-langganan"})
    public String ubahLanggananPenggunaPost(
            @RequestParam(name = "NasionalSubOptions") String nasional,
            @RequestParam(name = "InternasionalSubOptions") String internasional,
            Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        Pendaftar pendaftar = pendaftarService.getPendaftarByUsername(userSession.getUsername());
        pendaftar.setSubbedToBeritaNasional(Boolean.parseBoolean(nasional));
        pendaftar.setSubbedToBeritaInternasional(Boolean.parseBoolean(internasional));
        pendaftarService.createPendaftar(pendaftar);
        return "redirect:/berita/lihat";
    }

    @GetMapping(path = "/lihat/detail/{idBerita:[\\d]+}")
    public String detailBeritaPengguna(Model model,
                                       @PathVariable Integer idBerita) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        Berita berita = beritaService.getBerita(userSession.getUsername(), idBerita);
        model.addAttribute("berita", berita);
        return "berita/detailBerita";
    }

}
