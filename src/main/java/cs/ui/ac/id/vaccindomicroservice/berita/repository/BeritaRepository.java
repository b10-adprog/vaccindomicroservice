package cs.ui.ac.id.vaccindomicroservice.berita.repository;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.model.KategoriBerita;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeritaRepository extends JpaRepository<Berita, Integer> {
    @Override
    List<Berita> findAll();

    Berita findBeritaByIdBerita(Integer idBerita);

    Berita findBeritaByJudul(String judulBerita);

    void deleteBeritaByIdBerita(Integer idBerita);

    List<Berita> findBeritasByKategoriIs(KategoriBerita kategori);

}
