package cs.ui.ac.id.vaccindomicroservice.berita.service;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import java.util.List;

public interface BeritaService {
    public List<Berita> getAllBerita();

    public List<Berita> getAllBerita(String nikUser);

    public Berita getBerita(Integer idBerita);

    public Berita getBerita(String judulBerita);

    public Berita getBerita(String nikUser, Integer idBerita);

    public void deleteBerita(Integer idBerita);

    public void addBerita(Berita berita);
}
