package cs.ui.ac.id.vaccindomicroservice.pendaftar.service;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;

import java.util.List;

public interface PendaftarService {

    void createPendaftar(Pendaftar pendaftar);

    List<Pendaftar> getListPendaftar();

    Pendaftar getPendaftarByNik(String nik);

    Pendaftar getPendaftarByUsername(String username);
}
