package cs.ui.ac.id.vaccindomicroservice.pendaftar.model;

import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.model.HistoryPendaftaran;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="pendaftar")
@Data
@NoArgsConstructor
public class Pendaftar {

    @Id
    @Column(name = "username", updatable = false, nullable = false)
    private String username;

    @Column(name = "nik", updatable = false, nullable = false, unique = true)
    private String nik;

    @Column(name = "nama", updatable = false, nullable = false)
    private String nama;

    @Column(name= "password", updatable = false, nullable = false)
    private String password;

    @Column(name = "isAdmin", nullable = false)
    private Boolean isAdmin;

    @Column(name = "registered")
    private boolean registered;

    @Column(name = "firstVaccinated")
    private boolean firstVaccinated;

    @Column(name = "secondVaccinated")
    private boolean secondVaccinated;

    @OneToMany(cascade = CascadeType.ALL)
    private List<HistoryPendaftaran> historyPendaftarans = new ArrayList<>();

    @Column(name = "subbedToBeritaNasional")
    private boolean isSubbedToBeritaNasional;

    @Column(name = "subbedToBeritaInternasional")
    private boolean isSubbedToBeritaInternasional;

    public Pendaftar(String username, String nik, String nama, String password, boolean isAdmin){
        this.username = username;
        this.nik = nik;
        this.nama = nama;
        this.password = password;
        this.isAdmin = isAdmin;
        this.registered = false;
        this.firstVaccinated = false;
        this.secondVaccinated = false;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public String getNama() {
        return nama;
    }

    public String getNik() {
        return nik;
    }

    public boolean isFirstVaccinated() {
        return firstVaccinated;
    }

    public boolean isSecondVaccinated() {
        return secondVaccinated;
    }

    public String getPassword() {
        return password;
    }

    public void setIsAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
