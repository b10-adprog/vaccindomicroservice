package cs.ui.ac.id.vaccindomicroservice.pendaftar.service;

import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("customPendaftarDetailsService")
public class CustomPendaftarDetailsService implements UserDetailsService {

    @Autowired
    private PendaftarRepository repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Pendaftar pendaftar = repo.findPendaftarByUsername(username);
        if (pendaftar == null){
            throw new UsernameNotFoundException("Username Not Found");
        }
        return new CustomPendaftarDetails(pendaftar);
    }
}
