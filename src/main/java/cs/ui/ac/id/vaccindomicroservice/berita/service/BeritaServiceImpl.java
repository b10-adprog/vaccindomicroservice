package cs.ui.ac.id.vaccindomicroservice.berita.service;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.model.KategoriBerita;
import cs.ui.ac.id.vaccindomicroservice.berita.repository.BeritaRepository;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BeritaServiceImpl implements BeritaService {
    @Autowired
    BeritaRepository beritaRepo;
    @Autowired
    PendaftarService pendaftarService;

    @Override
    public Berita getBerita(Integer idBerita) {
        return beritaRepo.findBeritaByIdBerita(idBerita);
    }

    @Override
    public Berita getBerita(String judulBerita) {
        return beritaRepo.findBeritaByJudul(judulBerita);
    }

    @Override
    public Berita getBerita(String username, Integer idBerita) {
        return beritaRepo.findBeritaByIdBerita(idBerita);
    }

    @Override
    public List<Berita> getAllBerita() {
        return beritaRepo.findAll();
    }

    @Override
    public List<Berita> getAllBerita(String username) {
        Pendaftar user = pendaftarService.getPendaftarByUsername(username);
        List<Berita> listBerita = new ArrayList<>();
        if (user.isSubbedToBeritaNasional()) {
            List<Berita> listBeritaNasional = beritaRepo
                    .findBeritasByKategoriIs(KategoriBerita.NASIONAL);
            listBerita.addAll(listBeritaNasional);
        }
        if (user.isSubbedToBeritaInternasional()) {
            List<Berita> listBeritaInternasional = beritaRepo
                    .findBeritasByKategoriIs(KategoriBerita.INTERNASIONAL);
            listBerita.addAll(listBeritaInternasional);
        }
        return listBerita;
    }


    @Override
    @Transactional
    public void deleteBerita(Integer idBerita) {
        beritaRepo.deleteBeritaByIdBerita(idBerita);
    }

    @Override
    @Transactional
    public void addBerita(Berita berita) {
        beritaRepo.save(berita);
    }
}
