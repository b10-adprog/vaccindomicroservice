package cs.ui.ac.id.vaccindomicroservice.pendaftar.repository;

import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendaftarRepository extends JpaRepository<Pendaftar, String> {
    Pendaftar findByNik(String nik);

    Pendaftar findPendaftarByUsername(String username);
}