package cs.ui.ac.id.vaccindomicroservice.berita.controller;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.service.BeritaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/berita")
class RestBeritaControllerAdmin {
    @Autowired
    BeritaService beritaService;

    @GetMapping(path = "/cek", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> cekBeritaAdmin(@RequestParam String judulBerita) {
        Berita beritaInRepo = beritaService.getBerita(judulBerita);
        if (beritaInRepo == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(beritaInRepo.getJudul(), HttpStatus.OK);
    }

    @GetMapping(path = "/delete/{idBerita}")
    @ResponseBody
    public ResponseEntity<List<Berita>> deleteBeritaAdmin(@PathVariable Integer idBerita) {
        beritaService.deleteBerita(idBerita);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
