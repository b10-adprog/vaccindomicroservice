package cs.ui.ac.id.vaccindomicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccindoApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaccindoApplication.class, args);
    }

}
