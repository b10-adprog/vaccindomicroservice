package cs.ui.ac.id.vaccindomicroservice.hospital.controller;

import cs.ui.ac.id.vaccindomicroservice.hospital.model.Hospital;
import cs.ui.ac.id.vaccindomicroservice.hospital.service.HospitalService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class HospitalController {

    @Autowired
    HospitalService hospitalService;

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path = "/admin/hospital/input/")
    public String inputHospital(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        if (userSession.getIsAdmin()){
            model.addAttribute("hospital", new Hospital());
            model.addAttribute("user", userSession);
            return "hospital/inputHospital";
        }
        else {
            return "redirect:/";
        }
    }

    @PostMapping(path = "/admin/hospital/inputSave/")
    public String inputHospital(@Valid @ModelAttribute(name = "hospital") Hospital hospital, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "hospital/inputHospital";
        }
        hospitalService.addHospital(hospital);
        return "redirect:/admin/hospital/";
    }

    @GetMapping(path = "/admin/hospital/")
    public String listHospital(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        if (userSession.getIsAdmin()){
            List<Hospital> hospitalList = hospitalService.getAllHospital();
            model.addAttribute("listHospital", hospitalList);
            model.addAttribute("user", userSession);
            return "hospital/listHospital";
        }
        else {
            return "redirect:/";
        }
    }
    @GetMapping(path = "/hospital")
    public String listHospitalUser(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        List<Hospital> hospitalList = hospitalService.getAllHospital();
        model.addAttribute("listHospital", hospitalList);
        model.addAttribute("user", userSession);
        return "hospital/listHospital";
    }
}
