package cs.ui.ac.id.vaccindomicroservice.berita.controller;

import cs.ui.ac.id.vaccindomicroservice.berita.model.Berita;
import cs.ui.ac.id.vaccindomicroservice.berita.service.BeritaService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/admin/berita")
public class BeritaControllerAdmin {
    @Autowired
    BeritaService beritaService;

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path = {"", "/lihat"})
    public String lihatBeritaAdmin(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        if (userSession.getIsAdmin()) {
            List<Berita> listBerita = beritaService.getAllBerita();
            model.addAttribute("listBerita", listBerita);
            model.addAttribute("user", userSession);
            return "berita/listBeritaAsync";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping(path = "/tulisSync")
    public String tulisBeritaAdminSync(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        if (userSession.getIsAdmin()) {
            model.addAttribute("berita", new Berita());
            model.addAttribute("user", userSession);
            return "berita/tulisBeritaSync";
        } else {
            return "redirect:/";
        }
    }

    @PostMapping(path = "/simpan")
    public String simpanBeritaAdmin(@Valid @ModelAttribute(name = "berita") Berita berita,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "berita/tulisBeritaAsync";
        }
        berita.setTimeStamp(LocalDateTime.now());
        beritaService.addBerita(berita);
        return "redirect:/admin/berita/lihat/";
    }


    @GetMapping(path = "/deleteSync/{idBerita}")
    public String deleteBeritaAdmin(@PathVariable Integer idBerita, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        beritaService.deleteBerita(idBerita);
        return "redirect:/admin/berita/lihat";
    }

    @GetMapping(path = "/tulis")
    public String tulisBeritaAdminAsync(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        model.addAttribute("berita", new Berita());
        return "berita/tulisBeritaAsync";
    }
}
