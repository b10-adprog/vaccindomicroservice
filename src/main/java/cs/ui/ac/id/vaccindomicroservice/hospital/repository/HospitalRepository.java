package cs.ui.ac.id.vaccindomicroservice.hospital.repository;

import cs.ui.ac.id.vaccindomicroservice.hospital.model.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Integer> {
    @Override
    List<Hospital> findAll();
    Hospital findHospitalByIdHospital(Integer idHospital);
    Hospital findHospitalByHospitalName(String hospitalName);
}
