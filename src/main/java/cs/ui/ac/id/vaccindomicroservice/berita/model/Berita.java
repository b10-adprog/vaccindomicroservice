package cs.ui.ac.id.vaccindomicroservice.berita.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "berita")
@Data
@NoArgsConstructor
public class Berita {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_berita")
    private Integer idBerita;

    @Column(name = "kategori")
    private KategoriBerita kategori;

    @NotEmpty(message = "Judul berita tidak boleh kosong.")
    @Column(updatable = false, nullable = false)
    private String judul;

    @Column(updatable = false)
    private LocalDateTime timeStamp;

    @Column(name = "url_gambar", updatable = false)
    private String urlGambar;

    @NotEmpty(message = "Deskripsi tidak boleh kosong.")
    @Column(updatable = false, nullable = false, columnDefinition = "varchar")
    private String deskripsi;

    public Berita(String judul, KategoriBerita kategori, String urlGambar, String deskripsi) {
        this.judul = judul;
        this.kategori = kategori;
        this.urlGambar = urlGambar;
        this.deskripsi = deskripsi;
    }

    public void setTimeStamp(LocalDateTime now) {
        LocalDateTime dateTimeToMinutes = now.truncatedTo(ChronoUnit.MINUTES);
        this.timeStamp = dateTimeToMinutes;
    }

    public String getTimeStamp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return timeStamp == null ? "-" : timeStamp.format(formatter);
    }
}
