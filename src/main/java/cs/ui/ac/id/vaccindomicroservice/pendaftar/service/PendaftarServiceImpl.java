package cs.ui.ac.id.vaccindomicroservice.pendaftar.service;

import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.repository.PendaftarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PendaftarServiceImpl implements  PendaftarService{
    @Autowired
    private PendaftarRepository pendaftarRepository;

    @Override
    public void createPendaftar(Pendaftar pendaftar){
        BCryptPasswordEncoder passEncoder = new BCryptPasswordEncoder();
        String encodedPass = passEncoder.encode(pendaftar.getPassword());
        pendaftar.setPassword(encodedPass);
        pendaftar.setIsAdmin(false);

        if (pendaftar.getUsername().equals("admin")){
            pendaftar.setIsAdmin(true);
        }
        pendaftarRepository.save(pendaftar);
    }

    @Override
    public List<Pendaftar> getListPendaftar() {
        List<Pendaftar>listPendaftar = pendaftarRepository.findAll();
        return listPendaftar;
    }

    @Override
    public Pendaftar getPendaftarByNik(String nik) {
        return pendaftarRepository.findByNik(nik);
    }

    @Override
    public Pendaftar getPendaftarByUsername(String username) {
        return pendaftarRepository.findPendaftarByUsername(username);
    }


}
