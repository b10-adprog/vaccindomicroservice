package cs.ui.ac.id.vaccindomicroservice.pendaftar.service;

import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomPendaftarDetails implements UserDetails {

    private Pendaftar pendaftar;

    public CustomPendaftarDetails(Pendaftar pendaftar){
        this.pendaftar = pendaftar;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return pendaftar.getPassword();
    }

    @Override
    public String getUsername() {
        return pendaftar.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
