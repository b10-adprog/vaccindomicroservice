package cs.ui.ac.id.vaccindomicroservice.pendaftar.controller;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import java.util.List;

@Controller
public class PendaftarController {

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path="/registrasi")
    public String registrasiPendaftar(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        if (userSession != null){
            return "redirect:/";
        }
        else{
            model.addAttribute("pendaftar", new Pendaftar());
            model.addAttribute("user", userSession);
            return "pendaftar/registrasi";
        }
    }

    @PostMapping(path = "/registrasi")
    public String postPendaftar(@ModelAttribute Pendaftar pendaftar) {
        pendaftarService.createPendaftar(pendaftar);
        return "redirect:/login";
    }

    @GetMapping(path = "/pendaftar")
    public String getListPendaftar(Model model) {
        List<Pendaftar> listPendaftar= pendaftarService.getListPendaftar();
        model.addAttribute("listPendaftar", listPendaftar);
        return "pendaftar/listPendaftar";
    }

    @GetMapping(path = "/login")
    public String loginPendaftar(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());

        if (userSession != null) {
            return "redirect:/";
        } else {
            model.addAttribute("user", userSession);
            return "pendaftar/login";
        }
    }

    @GetMapping(path = "/logout")
    public String logoutPendaftar() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());

        if (userSession != null) {
            return "redirect:/logout";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping(path = "/")
    public String dashboard(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);
        return "dashboard";
    }

}
