package cs.ui.ac.id.vaccindomicroservice.historypendaftaran.controller;

import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.model.HistoryPendaftaran;
import cs.ui.ac.id.vaccindomicroservice.historypendaftaran.service.HistoryPendaftaranService;
import cs.ui.ac.id.vaccindomicroservice.hospital.service.HospitalService;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.model.Pendaftar;
import cs.ui.ac.id.vaccindomicroservice.pendaftar.service.PendaftarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
public class HistoryPendaftaranController {
    @Autowired
    HistoryPendaftaranService historyPendaftaranService;

    @Autowired
    HospitalService hospitalService;

    @Autowired
    PendaftarService pendaftarService;

    @GetMapping(path = "/admin/pendaftaran")
    public String getListPendaftaran(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());

        if (userSession.getIsAdmin()){
            List<HistoryPendaftaran> unfinishedPendaftaran =
                    historyPendaftaranService.getListPendaftaranUnfinished();
            model.addAttribute("unfinishedPendaftaran", unfinishedPendaftaran);
            model.addAttribute("user", userSession);
            return "historypendaftaran/listPendaftaran";
        }
        else {
            return "redirect:/";
        }

    }

    @PostMapping(path = "/pendaftaran/{idHistoryPendaftaran}", params = "setuju")
    public String postSetujui(Model model,
                              HttpServletRequest request,
                              @PathVariable int idHistoryPendaftaran)
            throws InterruptedException, ExecutionException, ParseException {
        if (historyPendaftaranService.getPendaftarSecondVaccinated(idHistoryPendaftaran)) {
            Future<Boolean> future = historyPendaftaranService.pendaftarFullyVaccinated();
            while (true) {
                if (future.isDone()) {
                    if (future.get()) {
                        historyPendaftaranService.getHistoryPendaftaran(idHistoryPendaftaran)
                                .setApproved(false);
                        model.addAttribute("noMore", true);
                    }
                    break;
                }
//                Thread.sleep(1000);
            }
        }
        else if(historyPendaftaranService.getHistoryPendaftaranPastTime(idHistoryPendaftaran)){
            Future<Boolean> futureTime = historyPendaftaranService.pendaftarTimePassed();
            while (true){
                if (futureTime.isDone()){
                    if (futureTime.get()){
                        historyPendaftaranService.getHistoryPendaftaran(idHistoryPendaftaran)
                                .setApproved(false);
                        model.addAttribute("noMore", true);
                    }
                }
                break;
            }
        }
        else {
            historyPendaftaranService.updateApprovalStatus(idHistoryPendaftaran, true);
        }

        historyPendaftaranService.updateFinishedStatus(idHistoryPendaftaran, true);
        return "redirect:/admin/pendaftaran";
    }

    @PostMapping(path = "/pendaftaran/{idHistoryPendaftaran}", params = "tolak")
    public String postTolak(HttpServletRequest request, @PathVariable int idHistoryPendaftaran) {
        historyPendaftaranService.updateApprovalStatus(idHistoryPendaftaran, false);
        historyPendaftaranService.updateFinishedStatus(idHistoryPendaftaran, true);
        return "redirect:/admin/pendaftaran";
    }

    @GetMapping(path="/daftar-vaksinasi")
    public String getFormPendaftaran(Model model) {
        model.addAttribute("semuaRS", hospitalService.getAllHospital());
        model.addAttribute("historyPendaftaran", new HistoryPendaftaran());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        model.addAttribute("user", userSession);

        return "historypendaftaran/daftarVaksinasi";
    }

    @PostMapping(path="/daftar-vaksinasi")
    public String postPendaftaran(@ModelAttribute(name = "historyPendaftaran") HistoryPendaftaran historyPendaftaran) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());
        String nik = userSession.getNik();
        historyPendaftaran.setNik(nik);
        historyPendaftaran.setApproved(false);
        historyPendaftaran.setFinished(false);
        historyPendaftaranService.daftarVaksinasi(historyPendaftaran);
        return "redirect:/profile";
    }

    @GetMapping(path = "/profile")
    public String profile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Pendaftar userSession = pendaftarService.getPendaftarByUsername(auth.getName());

        String nik = userSession.getNik();
        model.addAttribute("user", userSession);
        List<HistoryPendaftaran> allHistoryPendaftaran =
                historyPendaftaranService.getAllHistoryPendaftaranOfAProfile(nik);
        model.addAttribute("allHistoryPendaftaran", allHistoryPendaftaran);
        model.addAttribute("username", userSession.getUsername());
        return "historypendaftaran/profile";
    }
}
