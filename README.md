[![coverage report](https://gitlab.com/b10-adprog/vaccindomicroservice/badges/master/coverage.svg)](https://gitlab.com/b10-adprog/vaccindomicroservice/-/commits/master)

# Group Project Advanced Programming 2021
Kelompok B10:  
1. Mutia Rahmatun Husna (1706039622)  
2. Niti Cahyaning Utami (1906350894)  
3. Gita Permatasari Sujatmiko (1906400053)  
4. Steven Wiryadinata Halim (1906350622)  

## Deskripsi
Microservice untuk Main VaccIndo

## VaccIndo
***Tipe Aplikasi:*** Aplikasi Web  
***Nama Aplikasi:*** VaccIndo  
***Tech Stack:*** Spring Boot + Thymeleaf  
***Deskripsi Aplikasi:***
Merupakan suatu aplikasi berbasis website yang akan membantu masyarakat dalam melakukan pendaftaran vaksinasi dan donasi bantuan Covid-19. Pengguna aplikasi terbagi menjadi tenaga kesehatan sebagai administrator dan user biasa (user, pendaftar) yang merupakan masyarakat yang akan mendaftar vaksinasi ataupun berdonasi.